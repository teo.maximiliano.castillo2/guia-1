/*3) Mostrar en pantalla los números del 1 al 30 inclusive, pero solamente los pares. Usar bucles.*/
#include <stdio.h>

int main(){
    int numero;

    for (numero = 1; numero < 31; numero++)
    {
        if (numero % 2 == 0)
            printf ("\n%d",numero);
    }
    return(0);
}