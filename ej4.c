/*4) Mostrar en pantalla los números del 10 al 48 inclusive, pero solamente los múltiplos de 3. Usar bucles.*/

#include <stdio.h>

int main(){
    int numero;

    for (numero = 10; numero < 49; numero++)
    {
        if (numero % 3 == 0)
            printf ("\n%d",numero);
    }
    return(0);
}