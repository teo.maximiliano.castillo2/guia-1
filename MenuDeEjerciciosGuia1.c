#include <stdio.h> 
#include <conio.h>
#define CANTIDAD_DE_NOTAS 11
void menu();
void opciones();
void ListadoDeOpciones();
void selecciones ();
void promedio ();
void Valor235yValor23 ();
void SoloParesDel1al30 ();
void MultiplosDe3Del10al40 ();
void PositivosHastaNegativos();

int main() {
    menu();
    return (0);
}

void menu(){
    ListadoDeOpciones();
    opciones();
    }

void ListadoDeOpciones(){
    printf ("\nEl programa puede realizar las siguientes opciones:");
    printf ("\n1) Hacer un promedio con ciertas cantidad de notas");
    printf ("\n2) Ingresar valores hasta que se ingrese el 235 indicando el ingreso de 23");
    printf ("\n3) Se muestra en pantalla los numeros del 1 al 30 inclusive, pero solamente los pares");
    printf ("\n4) Se muestra en pantalla los numeros del 10 al 48 inclusive, pero solamente los múltiplos de 3");
    printf ("\n5) ingresa numeros enteros positivos, hasta que ingrese el numero -1 para indicar que finaliza el ingreso. Se mustra en pantalla cuantos numeros pares ingreso");
}

void opciones(){
    int valordelswitch;

    printf ("\n\nSeleccione una opcion:");
    scanf ("%d",&valordelswitch);
    selecciones (valordelswitch);
    getch ();
}

void selecciones (int valorRecivido){
    switch (valorRecivido)
    {
    case 1:
        promedio ();
        break;
    case 2:
        Valor235yValor23 ();
        break;
    case 3:
        SoloParesDel1al30 ();
        break;
    case 4:
        MultiplosDe3Del10al40 ();
        break;
    case 5:
        PositivosHastaNegativos();
        break;
    }
}

void promedio (){
    int suma=0;
    int nota=0;
    int i;

    for(i=0;i<CANTIDAD_DE_NOTAS;i++){
        printf ("\ningrese un numero entero:\n");
        scanf ("%d",&nota);

        suma=nota+suma;
    }
    printf ("\nEl promedio es: %f\n", (float) suma / CANTIDAD_DE_NOTAS);
}

void Valor235yValor23 (){
    int numero_ingresado;
    int contador_de_23 = 0;

    while (numero_ingresado != 235)
    {
        printf ("\nIngrese un numero:");
        scanf ("%d",&numero_ingresado);
        
        if (numero_ingresado == 23)
            contador_de_23 = 1;
    }
    if (numero_ingresado == 23)
    printf ("\nSe ingreso el valor 23\n");
}

void SoloParesDel1al30 (){
    int numero;

    for (numero = 1; numero < 31; numero++)
    {
        if (numero % 2 == 0)
            printf ("%d",numero);
    }
}

void MultiplosDe3Del10al40 (){
    int numero;

    for (numero = 10; numero < 49; numero++)
    {
        if (numero % 3 == 0)
            printf ("\n%d",numero);
    }
}

void PositivosHastaNegativos(){
    int numero_ingresado;
    int contador_de_numeros_pares = 0;

    while (numero_ingresado != -1)
    {
        printf ("Ingrese un numero:\n");
        scanf ("%d",&numero_ingresado);
        
        if (numero_ingresado % 2 == 0)
            contador_de_numeros_pares++;
    }
    printf ("Se ingresaron %d numeros pares\n", contador_de_numeros_pares);
}
