/*5) El usuario ingresará números enteros positivos (no sabemos cuántos), hasta que ingrese el número -1 para indicar que finaliza el ingreso.
Mostrar en pantalla cuántos números pares ingresó.*/
#include <stdio.h>

int main() {
    int numero_ingresado;
    int contador_de_numeros_pares = 0;

    while (numero_ingresado != -1)
    {
        printf ("Ingrese un numero:\n");
        scanf ("%d",&numero_ingresado);
        
        if (numero_ingresado % 2 == 0)
            contador_de_numeros_pares++;
    }
    printf ("Se ingresaron %d numeros pares\n", contador_de_numeros_pares);
    return(0);
}